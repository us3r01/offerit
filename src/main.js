import Vue from 'vue'
import App from './App.vue'


import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
Vue.use(BootstrapVue)


import VueRouter from 'vue-router'
Vue.use(VueRouter)

import LandPage from '../src/assets/pages/LandPage/index.vue'

const routes = [
  { path: '/LandPage', component: LandPage },
  { path: '/', redirect: '/LandPage' },
  { path: '/*', redirect: '/LandPage' },
]

const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})

import Vuex from 'vuex';
Vue.use(Vuex);
import store from './assets/store'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App),
  store,
})
